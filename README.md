# Module config

`.bashrc`:

```bash
source /path/to/lmod/init/profile
export MODULEPATH_ROOT=/path/to/modulefiles/<system>
export MODULEPATH=$MODULEPATH_ROOT/Core:$MODULEPATH
```

## Building libraries

### GCC 9.3.0

```bash
git clone git://gcc.gnu.org/git/gcc
cd gcc
git checkout releases/gcc-9.3.0
PREFIX=/opt/gcc/9.3.0 /path/to/build-gcc

git clone git://sourceware.org/git/binutils-gdb
cd binutils-gdb/binutils-gdb
git checkout binutils-2_32
PREFIX=/opt/gcc/9.3.0 /path/to/build-binutils
```

### OpenMPI

Download latest tarball...

```bash
tar xzf openmpi-3.1.6.tar.gz
cd openmpi-3.1.6
PREFIX=/opt/openmpi/3.1.6-gcc-9.3.0 /path/to/build-openmpi
```

### Hypre

```bash
git clone git@github.com:hypre-space/hypre
cd hypre
git checkout v2.19.0
PREFIX=/opt/hypre/2.19.0-gcc-9.3.0 /path/to/build-hypre
```
