whatis("Truchas "..myModuleVersion())

local fullName = myModuleFullName()
local hierA = hierarchyA(fullName,3)
local tplD = hierA[1]:gsub("/","-"):gsub("%.", "_")
--local mpiD = hierA[2]:gsub("/","-"):gsub("%.", "_")
--local compD = hierA[3]:gsub("/","-"):gsub("%.", "_")
local mpiD = hierA[2]
local compD = hierA[3]

local prefix = pathJoin("~/opt/lib/", compD, mpiD, tplD, fullName)
prepend_path("PATH", pathJoin(prefix, "bin"))
prepend_path("PYTHONPATH", pathJoin(prefix, "lib/python3.10/site-packages"))
