family("compiler")

local version = myModuleVersion()
whatis("Intel C/C++/Fortran "..version)

local prefix = pathJoin("/opt/intel/oneapi/compiler/", version)
local bindir1 = pathJoin(prefix, "bin")
prepend_path("PATH", bindir1)
prepend_path("LD_LIBRARY_PATH", pathJoin(prefix, "lib/"))
prepend_path("MANPATH", pathJoin(prefix, "share/man/"))

setenv("CC", pathJoin(bindir1, "icx"))
setenv("CXX", pathJoin(bindir1, "icpx"))
setenv("FC", pathJoin(bindir1, "ifx"))
--setenv("FC", pathJoin(bindir2, "ifx"))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
