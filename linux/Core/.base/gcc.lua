family("compiler")

local version = myModuleVersion()

whatis("GNU C/C++/Fortran "..version)

local prefix = pathJoin("~/opt/gcc", version)
local bindir = pathJoin(prefix, "bin")
local libdir = pathJoin(prefix, "lib64")
local mandir = pathJoin(prefix, "man")

prepend_path("PATH", bindir)
prepend_path("LD_LIBRARY_PATH", libdir)
prepend_path("MANPATH", mandir)

setenv("CC",  pathJoin(bindir, "gcc"))
setenv("CXX", pathJoin(bindir, "g++"))
setenv("FC",  pathJoin(bindir, "gfortran"))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
