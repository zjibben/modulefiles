family("compiler")

--local gccroot = "/opt/gcc/9.3.0"

local version = myModuleVersion()
whatis("Intel C/C++/Fortran "..version)

-- Workaround older Intel on systems with newer GCC.
-- pathJoin is used to expand ~ here.
setenv("GXX_INCLUDE", pathJoin("~/opt/gcc/9.3.0/include/c++/9.3.0"))
prepend_path("CPATH", "~/opt/gcc/9.3.0/include/c++/9.3.0/x86_64-linux-gnu/")

local prefix = pathJoin("/opt/intel", version, "compilers_and_libraries/linux")
local bindir = pathJoin(prefix, "bin/intel64")
prepend_path("PATH", bindir)
prepend_path("LD_LIBRARY_PATH", pathJoin(prefix, "lib/intel64"))
prepend_path("MANPATH", pathJoin(prefix, "man/common"))

-- setenv("GCC", gccroot)
-- prepend_path("PATH", pathJoin(gccroot, "bin"))
-- prepend_path("LD_LIBRARY_PATH", pathJoin(gccroot, "lib64"))

setenv("CC", pathJoin(bindir, "icc"))
setenv("CXX", pathJoin(bindir, "icpc"))
setenv("FC", pathJoin(bindir, "ifort"))

setenv("INTEL_LICENSE_FILE", "/opt/intel/licenses")

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
