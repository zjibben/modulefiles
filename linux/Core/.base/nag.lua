family("compiler")

local version = myModuleVersion()

whatis("NAG Fortran "..version.." with system GNU C/C++")

local prefix = pathJoin("/opt/NAG", version)
local bindir = pathJoin(prefix, "bin")
local libdir = pathJoin(prefix, "lib")
local mandir = pathJoin(prefix, "man")

prepend_path("PATH", bindir)
--prepend_path("LD_LIBRARY_PATH", libdir)
prepend_path("MANPATH", mandir)

--setenv("NAG_KUSARI_FILE", "128.165.87.4:")
setenv("NAG_KUSARI_FILE", "128.165.19.157:")

setenv("FC", pathJoin(bindir, "nagfor"))
-- use the system GNU C and C++ compilers
setenv("CC", "gcc")
setenv("CXX", "g++")

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
