family("compiler")

local version = myModuleVersion()
whatis("Intel C/C++/Fortran "..version)

local prefix = pathJoin("/opt/intel-managed/oneapi/compiler/", version, "linux")
local bindir1 = pathJoin(prefix, "bin")
local bindir2 = pathJoin(prefix, "bin/intel64")
prepend_path("PATH", bindir1)
prepend_path("PATH", bindir2)
prepend_path("LD_LIBRARY_PATH", pathJoin(prefix, "compiler/lib/intel64"))
prepend_path("MANPATH", pathJoin(prefix, "man/common"))

setenv("CC", pathJoin(bindir1, "icx"))
setenv("CXX", pathJoin(bindir1, "icpx"))
setenv("FC", pathJoin(bindir2, "ifort"))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
