local version = myModuleVersion()
whatis("Ninja "..version)
prepend_path("PATH", pathJoin("~/opt/ninja", version))
