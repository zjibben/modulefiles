whatis("Truchas "..myModuleVersion())

load("truchas-dev/gcc-12.3.0-mpich")

local prefix = "~/codes/telluride/truchas/install/linux.x86_64.gnu"
prepend_path("PATH", pathJoin(prefix, "bin"))
prepend_path("PYTHONPATH", pathJoin(prefix, "lib/python3.11/site-packages"))
