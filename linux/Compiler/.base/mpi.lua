family("mpi")

local fullName = myModuleFullName()

whatis(fullName)
local compM = hierarchyA(fullName,1)[1]
--local compD = compM:gsub("/","-"):gsub("%.", "_")
local compD = compM

local prefix = pathJoin("~/opt/lib", compD, fullName)
local bindir = pathJoin(prefix, "bin")
local libdir = pathJoin(prefix, "lib")
local incdir = pathJoin(prefix, "include")
local mandir = pathJoin(prefix, "share/man")

setenv("MPIHOME", prefix)
setenv("MPI_ROOT", prefix)
--setenv("MPI_LD_FLAGS", "-L"..libdir.." -lmpi")
--setenv("MPI_COMPILE_FLAGS", "-I"..incdir)

prepend_path("PATH", bindir)
prepend_path("LD_LIBRARY_PATH", libdir)
prepend_path("MANPATH", mandir)

-- make dependent modules available
local mver = myModuleVersion():match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "MPI", compM, myModuleName(), mver))
