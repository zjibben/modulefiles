local fullName = myModuleFullName()

whatis(fullName)
local hierA = hierarchyA(fullName,2)
local mpiM  = hierA[1]
local mpiD  = mpiM:gsub("/","-"):gsub("%.", "_")
local compM = hierA[2]
local compD = compM:gsub("/","-"):gsub("%.", "_")

local prefix = pathJoin("/opt/lib", compD, mpiD, fullName)
local libdir = pathJoin(prefix, "lib")
local incdir = pathJoin(prefix, "include")

setenv("HYPRE_DIR",  prefix)
setenv("HYPRE_ROOT", prefix)
setenv("HYPRE_INCLUDE_DIR", incdir)
setenv("HYPRE_LIBRARY_DIR", libdir)

-- make dependent modules available
local mver = myModuleVersion():match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Hypre", compM, mpiM, myModuleName(), mver))
