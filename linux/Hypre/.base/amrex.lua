local fullName = myModuleFullName()

whatis(fullName)
local hierA  = hierarchyA(fullName,3)
local hypreD = hierA[1]:gsub("/","-"):gsub("%.", "_")
local mpiD   = hierA[2]:gsub("/","-"):gsub("%.", "_")
local compD  = hierA[3]:gsub("/","-"):gsub("%.", "_")

local prefix = pathJoin("~/opt/lib", compD, mpiD, hypreD, fullName)

setenv("AMREX_ROOT", prefix)
setenv("AMREX_INCLUDE_DIR", pathJoin(prefix, "include"))
setenv("AMREX_LIBRARY_DIR", pathJoin(prefix, "lib"))
