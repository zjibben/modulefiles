whatis("Truchas "..myModuleVersion())

local fullName = myModuleFullName()
local hierA = hierarchyA(fullName,3)
local tplD = hierA[1]:gsub("/","-"):gsub("%.", "_")
local mpiD = hierA[2]:gsub("/","-"):gsub("%.", "_")
local compD = hierA[3]:gsub("/","-"):gsub("%.", "_")

local prefix = pathJoin("~/opt/truchas/", compD, mpiD, tplD, myModuleVersion())
prepend_path("PATH", pathJoin(prefix, "bin"))
prepend_path("PYTHONPATH", pathJoin(prefix, "lib/python3.8/site-packages"))
