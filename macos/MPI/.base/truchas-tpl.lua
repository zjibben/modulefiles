local fullName = myModuleFullName()

whatis(fullName)
local hierA = hierarchyA(fullName,2)
local mpiM  = hierA[1]
--local mpiD  = mpiM:gsub("/","-"):gsub("%.", "_")
local mpiD  = mpiM
local compM = hierA[2]
--local compD = compM:gsub("/","-"):gsub("%.", "_")
local compD = compM

setenv("TRUCHAS_TPL_DIR", pathJoin("~/opt/lib", compD, mpiD, fullName))

-- make dependent modules available
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "truchas", compM, mpiM, fullName))
