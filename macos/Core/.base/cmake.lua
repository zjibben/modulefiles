local version = myModuleVersion()
whatis("CMake "..version)
prepend_path("PATH", pathJoin("~/opt/cmake", version, "bin"))
