family("compiler")

local version = myModuleVersion()
whatis("Intel C/C++/Fortran "..version)

prepend_path("C_INCLUDE_PATH", "/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include")
prepend_path("CPLUS_INCLUDE_PATH", "/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include")
prepend_path("LD_LIBRARY_PATH", "/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib")

local prefix = pathJoin("/opt/intel/oneapi/compiler/", version, "mac")
local bindir = pathJoin(prefix, "bin/intel64")
prepend_path("PATH", bindir)
prepend_path("LD_LIBRARY_PATH", pathJoin(prefix, "compiler/lib/intel64"))
prepend_path("MANPATH", pathJoin(prefix, "man/common"))

setenv("CC", pathJoin(bindir, "icc"))
setenv("CXX", pathJoin(bindir, "icpc"))
setenv("FC", pathJoin(bindir, "ifort"))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
