family("compiler")

local version = myModuleVersion()
whatis("GNU C/C++/Fortran "..version)

local mver = version:match("(%d+)%.?")
setenv("CC",  pathJoin(bindir, "gcc-"..mver))
setenv("CXX", pathJoin(bindir, "g++-"..mver))
setenv("FC",  pathJoin(bindir, "gfortran-"..mver))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
