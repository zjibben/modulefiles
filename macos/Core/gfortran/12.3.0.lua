family("compiler")

local version = myModuleVersion()
whatis("Apple-Clang C/C++ + GNU Fortran "..version)

local mver = version:match("(%d+)%.?")
setenv("CC",  "clang")
setenv("CXX", "clang++")
-- setenv("CC",  "gcc")
-- setenv("CXX", "g++")
setenv("FC",  pathJoin(bindir, "gfortran-"..mver))

-- make dependent modules available
local mver = version:match("(%d+%.%d+)%.?")
local mroot = os.getenv("MODULEPATH_ROOT")
prepend_path("MODULEPATH", pathJoin(mroot, "Compiler", myModuleName(), mver))
