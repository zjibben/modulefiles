whatis("Truchas "..myModuleVersion())

load("truchas-dev")

local prefix = "~/codes/telluride/truchas/install/darwin.arm64.gnu"
prepend_path("PATH", pathJoin(prefix, "bin"))
prepend_path("PYTHONPATH", pathJoin(prefix, "lib/python3.12/site-packages"))
