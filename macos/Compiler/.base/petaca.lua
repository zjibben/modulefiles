local fullName = myModuleFullName()
local compM = hierarchyA(fullName,1)[1]
local compD = compM:gsub("/","-"):gsub("%.", "_")
setenv("PETACA_ROOT", pathJoin("/opt/lib", compD, fullName))
